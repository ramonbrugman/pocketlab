import AsyncStorage from '@react-native-community/async-storage';

const USERS_KEY = 'users';
const CURRENT_USER_KEY = 'current_user';

export default class AppSettings {
  static async users() {
    const users = await this._getValue(USERS_KEY, '[]');
    return JSON.parse(users);
  }

  static async addUser(host, accessToken) {
    let users = await this.users();
    users.push({host, accessToken});
    await this._setValue(USERS_KEY, JSON.stringify(users));
    return users.length - 1;
  }

  static async user(id) {
    const users = await this.users();
    return users[id];
  }

  static async updateUser(id, host, accessToken) {
    const users = await this.users();
    users[id] = {host, accessToken};
    await this._setValue(USERS_KEY, JSON.stringify(users));
  }

  static async currentUserId() {
    return await this._getValue(CURRENT_USER_KEY, 0);
  }

  static async currentUser() {
    const userId = await this._getValue(CURRENT_USER_KEY, 0);
    return await this.user(userId);
  }

  static async setCurrentUserId(userId) {
    await this._setValue(CURRENT_USER_KEY, userId.toString());
    return await this.user(userId);
  }

  static async deleteUser(userId) {
    const users = await this.users();
    users.splice(userId, 1);
    await this._setValue(USERS_KEY, JSON.stringify(users));
  }

  static async _getValue(key, defaultValue) {
    try {
      const value = await AsyncStorage.getItem(key);
      if (value === null) {
        return defaultValue;
      } else {
        return value;
      }
    } catch (e) {
      return defaultValue;
    }
  }

  static async _setValue(key, value) {
    await AsyncStorage.setItem(key, value);
  }
}
