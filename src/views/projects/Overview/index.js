import React, {useEffect, useState, useCallback} from 'react';
import {StyleSheet, Image, ScrollView, View, Text} from 'react-native';
import {withTitle} from '@components/Title';
import GitLabAPI from '../../../GitLabAPI';
import {useParams} from 'react-router-native';
import {Button, ActivityIndicator, Paragraph} from 'react-native-paper';

const readmeNames = ['readme', 'readme.md'];

const styles = StyleSheet.create({
  projectDescription: {
    padding: 10,
  },
});

const Overview = props => {
  const {project: projectID} = useParams();
  const [project, setProject] = useState(null);
  useEffect(() => {
    const gitlab = new GitLabAPI();
    gitlab.project(projectID).then(response => {
      props.setTitle(`${response.name_with_namespace}`);
      if (response.description === '') {
        response.description = 'This project does not have a description';
      }
      setProject(response);
    });
  }, [projectID, props]);

  const starProject = useCallback(() => {
    const gitlab = new GitLabAPI();
    gitlab.starProject(projectID).then(result => {
      let newProject = {...project, star_count: project.star_count + 1};
      if (result !== false) {
        setProject(newProject);
      }
    });
  }, [projectID, project]);

  if (project === null) {
    return <ActivityIndicator animating={true} size="large" />;
  }
  return (
    <View>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          padding: 5,
        }}>
        <Button icon="star-outline" onPress={starProject} mode="contained">
          {project.star_count}
        </Button>
        <Button icon="source-fork" mode="contained">
          {project.forks_count}
        </Button>
      </View>
      <Paragraph style={styles.projectDescription}>
        {project.description}
      </Paragraph>
    </View>
  );
};

export default withTitle(Overview);
