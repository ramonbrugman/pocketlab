import React, {useState, useEffect} from 'react';
import {View, StyleSheet, Alert, Text} from 'react-native';
import {
  withTheme,
  TextInput,
  Card,
  Button,
  FAB,
  Headline,
  Paragraph,
} from 'react-native-paper';
import {useHistory, useRouteMatch} from 'react-router-native';
import {withTitle} from '@components/Title';
import AppSettings from '@modules/AppSettings';
import GitLabAPI from '../../GitLabAPI.js';

const NewAccount = props => {
  const colors = props.theme.colors;
  const [host, setHost] = useState('https://gitlab.com');
  const [accessToken, setAccessToken] = useState('');
  const [showDeleteConfirmation, setShowDeleteConfirmation] = useState(false);
  const history = useHistory();
  const match = useRouteMatch('/accounts/edit/:id') || {params: {}};
  props.setTitle(
    match.params.id !== undefined ? 'Update Account' : 'Add Account',
  );

  useEffect(() => {
    if (match.params.id !== undefined) {
      AppSettings.user(match.params.id).then(user => {
        setHost(user.host);
        setAccessToken(user.accessToken);
      });
    }
  }, [match.params.id]);

  const addAccount = async () => {
    if (match.params.id === undefined) {
      const userId = await AppSettings.addUser(host, accessToken);
      await AppSettings.setCurrentUserId(userId);
      const gitlab = new GitLabAPI();
      gitlab.setConnection(host, accessToken);
      history.replace('/');
    } else {
      await AppSettings.updateUser(match.params.id, host, accessToken);
      const userId = await AppSettings.currentUserId();
      if (userId === match.params.id) {
        const gitlab = new GitLabAPI();
        gitlab.setConnection(host, accessToken);
      }
    }
  };

  const deleteAccount = async () => {
    Alert.alert(
      'Are You Sure?',
      'Are you sure that you want to delete this account?',
      [
        {text: 'Cancel', style: 'cancel'},
        {
          text: 'OK',
          onPress: async () => {
            await AppSettings.deleteUser(match.params.id);
            const gitlab = new GitLabAPI();
            if (gitlab.accessToken === accessToken) {
              gitlab.setConnection(undefined, undefined);
            }
            history.goBack();
          },
        },
      ],
    );
  };

  return (
    <Card>
      <Card.Content>
        <TextInput
          label="GitLab Server"
          keyboardType="url"
          autoCapitalize="none"
          value={host}
          onChangeText={setHost}
          mode="outlined"
        />
        <TextInput
          label="Access Token"
          mode="outlined"
          value={accessToken}
          onChangeText={setAccessToken}
        />

        <Headline>How to Create an Access Token</Headline>
        <Paragraph>
          1. Log in to GitLab.{'\n'}
          2. In the upper-right corner, click your avatar and select Settings.
          {'\n'}
          3. On the User Settings menu, select Access Tokens.{'\n'}
          4. Choose a name and optional expiry date for the token.{'\n'}
          5. Choose the "API" scope.{'\n'}
          6. Click the Create personal access token button.
        </Paragraph>
      </Card.Content>
      <Card.Actions>
        {match.params.id !== undefined && (
          <Button onPress={deleteAccount}>Delete</Button>
        )}
        <Button mode="contained" onPress={addAccount}>
          {match.params.id !== undefined ? 'Save' : 'Add'}
        </Button>
      </Card.Actions>
    </Card>
  );
};

export default withTitle(withTheme(NewAccount));
