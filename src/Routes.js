import React from 'react';
import {Route, Switch, Redirect} from 'react-router-native';
import Todos from './views/Todos';
import Issue from './views/projects/Issues/index.js';
import EditIssue from './views/projects/Issues/EditIssue.js';
import ListProjects from './views/projects/List.js';
import ViewProject from './views/projects/ViewProject.js';
import ViewBlob from './views/projects/Repo/ViewBlob.js';
import ViewPipeline from './views/projects/Pipelines/View.js';
import ViewJob from './views/projects/Jobs/ViewLog.js';
import ViewMergeRequests from './views/projects/MergeRequests/View.js';
import NewAccount from './views/accounts/NewAccount.js';
import SelectAccount from './views/accounts/SelectAccount.js';
import SettingsIndex from './views/settings/Index.js';
import SettingsLicenses from './views/settings/Licenses.js';
import ViewLicense from './views/settings/ViewLicense.js';
import ListUserMergeRequests from './views/MergeRequests.js';
import UserAccountRedirect from '@components/UserAccountRedirect.js';

const Routes = () => {
  return (
    <>
      <Route path="" component={UserAccountRedirect} />
      <Switch>
        <Route exact path="/" component={Todos} />
        <Route exact path="/merge-requests" component={ListUserMergeRequests} />
        <Route exact path="/projects" component={ListProjects} />
        <Route
          exact
          path="/projects/:project/issues/:issueId"
          component={Issue}
        />
        <Route
          exact
          path="/projects/:project/new-issue"
          component={EditIssue}
        />
        <Route
          exact
          path="/projects/:project/view-blob/:sha"
          component={ViewBlob}
        />
        <Route
          exact
          path="/projects/:project/pipelines/:pipeline"
          component={ViewPipeline}
        />
        <Route exact path="/projects/:project/jobs/:job" component={ViewJob} />
        <Route
          exact
          path="/projects/:project/merge_request/:merge_request"
          component={({match}) => (
            <Redirect
              to={`/projects/${match.params.project}/merge_request/${
                match.params.merge_request
              }/overview`}
            />
          )}
        />
        <Route
          exact
          path="/projects/:project/merge_request/:merge_request/:tab"
          component={ViewMergeRequests}
        />
        <Route exact path="/projects/:project/:tab" component={ViewProject} />

        <Route exact path="/accounts/new" component={NewAccount} />
        <Route exact path="/accounts/edit/:id" component={NewAccount} />
        <Route exact path="/accounts/select/:id" component={SelectAccount} />

        <Route exact path="/settings" component={SettingsIndex} />
        <Route exact path="/settings/licenses" component={SettingsLicenses} />
        <Route path="/settings/license/:id" component={ViewLicense} />
      </Switch>
    </>
  );
};

export default Routes;
