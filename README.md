# PocketLab

The third party mobile app that helps you manage your GitLab projects whilst on the go

[![alt text](/doc/images/app_store_badge.png "Download on the App Store")](https://apps.apple.com/us/app/pocketlab-for-gitlab/id1493904816?ls=1)
[![alt text](/doc/images/google-play-badge.png "Get it on Google Play")](https://play.google.com/store/apps/details?id=com.pocketlab)

## Software Stack
PocketLab is built using [React Native](https://facebook.github.io/react-native/). React Native uses JavaScript to enable a single codebase for both iOS and Android

## Development Installation
- Make sure you have either xcode or Android Studio installed
- Install the [yarn](https://yarnpkg.com/lang/en/) package manager
- Run `yarn install`
- Use `yarn ios` or `yarn android` to run PocketLab on the respective simulator
